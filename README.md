**MOSTLY DEPRECATED**

La majorité de ce répertoire a été migrée ailleurs.

Il reste le script `cluster_footprint.py` qui n'est pas migré mais il n'a pas grand intéret et peut être réécrit facilement à partir des librairies de scanettetools




# Clustering et évaluation de traces sur des mutants

avant de comencer :
+ `git submodule update --init` pour télécharger la librairie agilkia avec
        HMM et les traces scanettes
+ copier le répertoire jumble fourni par Mark dans le sous-dossier scanette


ensuite, lancer le script `run_scanette.py`
Le fichier de traces à utiliser est inscrit à la fin du script, ainsi que le nombre de clusters à créer


