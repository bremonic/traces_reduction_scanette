import agilkia_hmm.agilkia as agilkia
import tempfile
import os
import csv
from datetime import datetime
from pathlib import Path
import subprocess
import random

repo_path=os.path.abspath( os.path.curdir)

#method copied from Mark's example in agilkia repository
def read_traces_csv(path: Path) -> agilkia.TraceSet:
    # print("now=", datetime.now().timestamp())
    with path.open("r") as input:
        trace1 = agilkia.Trace([])
        for line in csv.reader(input):
            # we ignore the line id.
            timestr = line[1].strip()
            timestamp = datetime.fromtimestamp(int(timestr) / 1000.0)
            # print(timestr, timestamp.isoformat())
            sessionID = line[2].strip()
            objInstance = line[3].strip()
            action = line[4].strip()
            paramstr = line[5].strip()
            result = line[6].strip()
            # now we identify the main action, inputs, outputs, etc.
            if paramstr == "[]":
                inputs = {}
            else:
                if  paramstr.startswith("[") and paramstr.endswith("]"):
                    paramstr = paramstr[1:-1]
                inputs = {"param" : paramstr}
            if result == "?":
                outputs = {}
            else:
                outputs = {'Status': float(result)}
            others = {
                    'timestamp': timestamp,
                    'sessionID': sessionID,
                    'object': objInstance
                    }
            event = agilkia.Event(action, inputs, outputs, others)
            trace1.append(event)
    traceset = agilkia.TraceSet([])
    traceset.append(trace1)
    return traceset

def trace_to_csv(trace_set:agilkia.TraceSet,filename:str):
    events=[]
    for trace in trace_set:
        for event in trace:
            events.append(event)
    events.sort(key=lambda e:e.meta_data['timestamp'])
    with open (filename,"w") as f:
        writer=csv.writer(f)
        line_id=0
        for event in events:
            timestamp=event.meta_data['timestamp'].timestamp()*1000
            sessionId=event.meta_data['sessionID']
            objInstance=event.meta_data['object']
            action=event.action
            if 'param' in event.inputs:
                paramstr='['+str(event.inputs['param'])+']'
            else :
                paramstr="[]"
            result=event.outputs.get('Status','?')


            writer.writerow([line_id,timestamp,sessionId,objInstance,action,paramstr,result])
            line_id+=1




def replay(trace_set :agilkia.TraceSet,show_output=False):
    process_args={}
    if not show_output:
        process_args['stdout']=subprocess.DEVNULL
        process_args['stderr']=subprocess.DEVNULL
    run_dir=os.path.join(repo_path,"scanette","replay")
    with tempfile.NamedTemporaryFile(delete=False,suffix=".json") as temp:
        trace_path=temp.name
    trace_set.save_to_json(Path(trace_path))
    process=subprocess.Popen([
        "java", "-cp", "ScanetteTestReplay.jar:json-simple.jar:junit-4.12.jar:scanette.jar", "fr.philae.ScanetteTraceExecutor",trace_path
],cwd=run_dir,**process_args)
    process.communicate()
    if process.returncode != 0:
        print("trace is not accepted by normal implementation.")
    else:
        mutants=[]
        for f in os.listdir(run_dir):
            if f.startswith("scanette-mu"):
                mutants.append(os.path.join(run_dir,f))
        killed=[]
        survide=[]
        errors=[]
        for mutant in mutants:
            process=subprocess.Popen([
        "java", "-cp", "ScanetteTestReplay.jar:json-simple.jar:junit-4.12.jar:"+mutant, "fr.philae.ScanetteTraceExecutor",trace_path
],cwd=run_dir,**process_args)
            process.communicate()
            if process.returncode==0:
                survide.append(mutant)
            elif process.returncode==1:
                killed.append(mutant)
            else:
                errors.append(mutant)
        print("killed {} on {} mutants.".format(len(killed),len(mutants)))
        if len(survide)!=0:
            print("surviving : ", survide)
        if len (errors)!=0:
            print("errors : ",errors)

    #now run with jumble :
    process_args={}
    #if not show_output:
    #    process_args['stdout']=subprocess.DEVNULL
    #    process_args['stderr']=subprocess.DEVNULL
    run_dir=os.path.join(repo_path,"scanette","jumble","replay")
    trace_to_csv(trace_set,os.path.join(run_dir,"tests.csv"))
    trace_set.save_to_json(os.path.join(run_dir,"agilkia_trace.json"))
    process=subprocess.Popen(["./run_jumble.sh"],cwd=run_dir,**process_args)
    process.communicate()




def extract_traces(trace_set:agilkia.TraceSet,number:int)->agilkia.TraceSet:
    if len(trace_set)<number:
        return traces_set
    hmm_cluster=agilkia.HMM_ClusterAlgo(K=number,states=10)
    hmm_cluster.fit(trace_set)
    clusters_ids=hmm_cluster.predict(trace_set)
    clusters=[]
    for i in range(max(clusters_ids)+1):
        clusters.append([])
    for i in range(len(trace_set)):
        clusters[clusters_ids[i]].append(i)
    chosen=[]
    for i in range(len(clusters)):
        chosen.append(random.choice(clusters[i]))
    chosenTraces=agilkia.TraceSet([])
    for id_ in chosen:
        chosenTraces.append(trace_set[id_])
    print(chosen)
    return chosenTraces
    



trace_file=os.path.join(repo_path,"scanette","traces","1026-steps.csv")
#trace_file=os.path.join(repo_path,"scanette","traces","100043-steps.csv")
#trace_file=os.path.join(repo_path,"scanette","traces","functional-tests.csv")
#trace_file=os.path.join(repo_path,"scanette","traces","200035-steps.csv")

number=7

trace_set=read_traces_csv(Path(trace_file))
trace_set = trace_set.with_traces_grouped_by("sessionID", property=True)
trace_set=extract_traces(trace_set,number)
new_file=trace_file[:-4]+"__"+str(number)+"_traces"
trace_to_csv(trace_set,new_file+".csv")
trace_set.save_to_json(new_file+".json")
replay(trace_set)
